﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Application.Seedwork
{
	[AttributeUsage(AttributeTargets.Class,AllowMultiple = true)]
	public class ServiceApplicationErrorHandlerAttribute : Attribute, IServiceBehavior
	{
		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			//not implemented
		}

		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
			BindingParameterCollection bindingParameters)
		{
			//
		}

		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
		{
			if(serviceHostBase != null && serviceHostBase.ChannelDispatchers.Any())
			{
				foreach(ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
				{
					channelDispatcher.ErrorHandlers.Add(new ServiceApplicationErrorHandler());
				}
			}
		}
	}
}
