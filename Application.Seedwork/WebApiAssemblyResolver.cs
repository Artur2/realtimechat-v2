﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Http.Dispatcher;

namespace Application.Seedwork
{
	public class WebApiAssemblyResolver : IAssembliesResolver
	{
		public ICollection<Assembly> GetAssemblies()
		{
			return BuildManager.GetReferencedAssemblies().OfType<Assembly>().ToList();
		}
	}
}
