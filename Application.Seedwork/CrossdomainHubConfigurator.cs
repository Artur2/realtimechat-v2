﻿using Microsoft.AspNet.SignalR;

namespace Application.Seedwork {

	public class CrossdomainHubConfigurator : HubConfiguration {

		public CrossdomainHubConfigurator() {
			EnableCrossDomain = true;
			EnableJavaScriptProxies = true;
		}
	}
}
