﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Application.Seedwork
{
	[DataContract(Name = "ServiceErrorMessage",Namespace = "RealtimeChat.Application.Seedwork")]
	public class ApplicationServiceError
	{
		[DataMember(Name = "ErrorMessage")]
		public string ErrorMessage { get; set; }
	}
}
