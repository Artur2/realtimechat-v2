﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Logging;
using Microsoft.Practices.Unity;

namespace Application.Seedwork
{
	public class ServiceApplicationErrorHandler : IErrorHandler
	{
		public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
		{
			if(error is FaultException<ApplicationServiceError>)
			{
				MessageFault messageFault = ( (FaultException<ApplicationServiceError>)error ).CreateMessageFault();

				fault = Message.CreateMessage(version, messageFault, ( (FaultException<ApplicationServiceError>)error ).Action);
			}
			else
			{
				var applicationServiceError = new ApplicationServiceError()
				{
					ErrorMessage = error.Message
				};

				var defaultFaultException = new FaultException<ApplicationServiceError>(applicationServiceError);

				var defaultMessageFault = defaultFaultException.CreateMessageFault();

				fault = Message.CreateMessage(version, defaultMessageFault, defaultFaultException.Action);
			}
		}

		public bool HandleError(Exception error)
		{
			var logger = DIContainer.Current.Resolve<ILoggerFactory>().Create();

			logger.WriteError(error.Message);

			return true;
		}
	}
}
