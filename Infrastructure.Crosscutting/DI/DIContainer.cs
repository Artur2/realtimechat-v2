﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Hosting;
using Microsoft.Practices.Unity;

namespace Infrastructure.Crosscutting.DI
{
	public static class DIContainer
	{
		private static object LockObject = new object();

		private static IUnityContainer container;

		static DIContainer()
		{
			lock(LockObject)
			{
				container = new UnityContainer();
				RegisterTypes();
			}
		}

		public static IUnityContainer Current
		{
			get
			{
				return container;
			}
		}

		private static void RegisterTypes()
		{
			var types = GetRegistrationTypes();

			foreach(var type in types)
			{
				var obj = (IUnityContainerTypeRegistrationProvider)Activator.CreateInstance(type);
				obj.Register(container);
			}
		}

		private static IEnumerable<Type> GetRegistrationTypes()
		{
			var domainAssemblies = new HashSet<Assembly>();

			var referencedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

			var executionAssembly = AppDomain.CurrentDomain.DomainManager.EntryAssembly;

			foreach(var referencedAssembly in referencedAssemblies)
			{
				domainAssemblies.Add(referencedAssembly);
			}

			domainAssemblies.Add(executionAssembly);

			var list = new List<Type>();

			foreach(var domainAssembly in domainAssemblies)
			{
				try
				{
					var types =
						domainAssembly.GetTypes().Where(_ => _.GetInterface(typeof(IUnityContainerTypeRegistrationProvider).Name) != null);

					list.AddRange(types);
				}
				catch(Exception ex)
				{
					//todo: impl logging
					Trace.TraceError(ex.Message);
				}
			}

			return list;
		}
	}
}
