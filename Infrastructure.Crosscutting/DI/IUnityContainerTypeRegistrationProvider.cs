﻿using Microsoft.Practices.Unity;

namespace Infrastructure.Crosscutting.DI
{
	/// <summary>
	/// Main interface contract for type register
	/// <remarks>
	/// Create class which derive this type and use it for type registration
	/// </remarks>
	/// </summary>
	public interface IUnityContainerTypeRegistrationProvider
	{
		void Register(IUnityContainer container);
	}
}
