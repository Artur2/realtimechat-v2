﻿namespace Infrastructure.Crosscutting.Logging
{
	/// <summary>
	/// Base contract for logger factory
	/// </summary>
	public interface ILoggerFactory
	{
		ILogger Create();
	}
}
