﻿namespace Infrastructure.Crosscutting.Logging.Trace
{
	public class TraceLogger : ILogger
	{
		public void WriteInfo(string msg)
		{
			System.Diagnostics.Trace.WriteLine(msg);
		}

		public void WriteError(string errorMsg)
		{
			System.Diagnostics.Trace.TraceError(errorMsg);
		}
	}
}
