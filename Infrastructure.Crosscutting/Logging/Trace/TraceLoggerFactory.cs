﻿using Infrastructure.Crosscutting.DI;
using Microsoft.Practices.Unity;

namespace Infrastructure.Crosscutting.Logging.Trace
{
	public class TraceLoggerFactory : ILoggerFactory
	{
		public ILogger Create()
		{
			return DIContainer.Current.Resolve<ILogger>();
		}
	}
}
