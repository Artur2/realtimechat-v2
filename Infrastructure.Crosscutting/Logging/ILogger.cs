﻿namespace Infrastructure.Crosscutting.Logging
{
	/// <summary>
	/// Base contract for logger
	/// </summary>
	public interface ILogger
	{
		void WriteInfo(string msg);

		void WriteError(string errorMsg);
	}
}
