﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.Validation
{
	public interface IValidator
	{
		/// <summary>
		/// Throws if object is null
		/// </summary>
		/// <param name="item">Check item</param>
		void ThrowIfNull(object item);

		/// <summary>
		/// Check condition and if not matched throws exception
		/// </summary>
		/// <typeparam name="TEntity"></typeparam>
		/// <param name="predicate">Expression</param>
		/// <param name="targetItem">Checking object</param>
		void ThrowIfNotSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem);

		/// <summary>
		/// Check condition
		/// </summary>
		/// <typeparam name="TEntity"></typeparam>
		/// <param name="predicate">Expression</param>
		/// <param name="targetItem">Checking object</param>
		/// <returns>True if satisfy, and false if not</returns>
		bool IsSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem);

		/// <summary>
		/// Log if condition not satisfied
		/// </summary>
		/// <typeparam name="TEntity">{TObject}</typeparam>
		/// <param name="predicate">Conditional expression</param>
		/// <param name="targetItem">object which will be check</param>
		/// <param name="logMessage">Message writen if condition is not satisfied</param>
		/// <returns>True if condition matched, and false if not</returns>
		bool LogIfNotSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem, string logMessage);
	}
}
