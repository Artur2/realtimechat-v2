﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Logging;
using Microsoft.Practices.Unity;

namespace Infrastructure.Crosscutting.Validation
{
	public class Validator : IValidator
	{
		public void ThrowIfNull(object item)
		{
			if(item == null)
			{
				throw new ArgumentNullException("item");
			}
		}

		public void ThrowIfNotSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem)
		{
			if(!predicate(targetItem))
			{
				throw new Exception("not satisfied by condition");
			}
		}

		public bool IsSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem)
		{
			return predicate(targetItem);
		}

		public bool LogIfNotSatisfied<TEntity>(Func<TEntity, bool> predicate, TEntity targetItem, string logMessage)
		{
			var condition = predicate(targetItem);

			if(!condition)
			{
				DIContainer.Current.Resolve<ILoggerFactory>().Create().WriteError(logMessage);
			}

			return condition;
		}
	}
}
