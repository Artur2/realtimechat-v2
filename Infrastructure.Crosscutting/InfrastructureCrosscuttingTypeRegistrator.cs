﻿using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Logging;
using Infrastructure.Crosscutting.Logging.Trace;
using Infrastructure.Crosscutting.Validation;
using Microsoft.Practices.Unity;

namespace Infrastructure.Crosscutting
{
	public class InfrastructureCrosscuttingTypeRegistrator : IUnityContainerTypeRegistrationProvider
	{
		public void Register(IUnityContainer container)
		{
			container.RegisterType<ILogger, TraceLogger>();
			container.RegisterType<ILoggerFactory, TraceLoggerFactory>();
			container.RegisterType<IValidator, Validator>();
		}
	}
}
