﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceProcess;

namespace DistributedChatService
{
	public partial class DistributedChatService : ServiceBase
	{
		internal static ServiceHost serviceHost = null;

		public DistributedChatService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			WriteLog("Service started", EventLogEntryType.Information);

			if(serviceHost != null)
			{
				serviceHost.Close();
			}

			serviceHost = new ServiceHost(typeof(DistributedServices.DistributedChatService));

			serviceHost.Open();
		}

		protected override void OnStop()
		{
			if(serviceHost != null)
			{
				serviceHost.Close();
				serviceHost = null;
			}
			WriteLog("Service stoped", EventLogEntryType.Information);
		}

		private void WriteLog(string message, EventLogEntryType type)
		{
			if(EventLog != null)
			{
				base.EventLog.WriteEntry(message, type);
			}
		}
	}
}
