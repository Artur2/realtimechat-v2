﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DistributedChatService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			try
			{
				ServiceBase[] ServicesToRun;
				ServicesToRun = new ServiceBase[]
				{
					new DistributedChatService()
				};
				ServiceBase.Run(ServicesToRun);
			}
			catch(Exception ex)
			{
				if(!EventLog.SourceExists("DistributedChatService"))
				{
					EventLog.CreateEventSource("DistributedChatService", "ExceptionLogging");
				}

				EventLog.WriteEntry("DistributedChatService", ex.Message + "\n" + ex.StackTrace);
			}
		}
	}
}
