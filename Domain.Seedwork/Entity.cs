﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace Domain.Seedwork
{

	[Serializable]
	public abstract class Entity
	{
		[Key]
		public Guid Id { get; set; }
	}
}
