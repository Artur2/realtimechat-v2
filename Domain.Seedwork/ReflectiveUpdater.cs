﻿using System.Diagnostics;
using System.Linq;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Logging;
using Microsoft.Practices.Unity;

namespace Domain.Seedwork
{
	public static class ReflectiveUpdater
	{
		public static void UpdateProperties<T>(this T target, T source)
		{
			var stopwatch = new Stopwatch();

			stopwatch.Start();
			
			var targetProperties = target.GetType().GetProperties().Where(_ => _.SetMethod != null);

			var sourceProperties = source.GetType().GetProperties().Where(_ => _.SetMethod != null);

			stopwatch.Stop();

			DIContainer.Current.Resolve<ILoggerFactory>().Create().WriteInfo("extracting properties : " + stopwatch.ElapsedMilliseconds);

			foreach(var targetProperty in targetProperties)
			{
				var equalSourceProperty = sourceProperties.FirstOrDefault(_ => _.Name.Equals(targetProperty.Name));

				if(equalSourceProperty != null)
				{
					targetProperty.SetValue(target, equalSourceProperty.GetValue(source));
				}
			}
		}
	}
}
