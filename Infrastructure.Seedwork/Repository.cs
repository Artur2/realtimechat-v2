﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Logging;
using Infrastructure.Crosscutting.Validation;
using Microsoft.Practices.Unity;

namespace Infrastructure.Seedwork
{
	public class Repository<TEntity> : IRepository<TEntity>
		where TEntity : Entity
	{
		#region IDisposableMembers


		public void Dispose()
		{
			throw new NotImplementedException();
		}


		#endregion

		#region Members

		private IQueryableUnitOfWork _unitOfWork;

		#endregion

		#region Constructor

		public Repository(IQueryableUnitOfWork unitOfWork)
		{
			DIContainer.Current.Resolve<IValidator>().ThrowIfNull(unitOfWork);

			_unitOfWork = unitOfWork;
		}

		#endregion

		#region IRepositoryMembers

		public IUnitOfWork UnitOfWork
		{
			get
			{
				return _unitOfWork;
			}
		}

		public void Add(TEntity item)
		{
			if(DIContainer.Current.Resolve<IValidator>()
				.LogIfNotSatisfied(_ => _ != null, item, "item is null" + Environment.NewLine + Environment.StackTrace))
			{
				GetSet().Add(item);
			}
		}

		public void Remove(TEntity item)
		{
			if(DIContainer.Current.Resolve<IValidator>()
				.LogIfNotSatisfied(_ => _ != null, item, "item is null" + Environment.NewLine + Environment.StackTrace))
			{
				_unitOfWork.Attach(item);

				GetSet().Remove(item);
			}
		}

		public void Modify(TEntity item)
		{
			if(DIContainer.Current.Resolve<IValidator>()
				.LogIfNotSatisfied(_ => _ != null, item, "item is null" + Environment.NewLine + Environment.StackTrace))
			{
				_unitOfWork.SetModified(item);
			}
		}

		public void TrackItem(TEntity item)
		{
			if(DIContainer.Current.Resolve<IValidator>()
				.LogIfNotSatisfied(_ => _ != null, item, "item is null" + Environment.NewLine + Environment.StackTrace))
			{
				_unitOfWork.Attach(item);
			}
		}

		public void Merge(TEntity persisted, TEntity current)
		{
			_unitOfWork.ApplyCurrentValues(persisted, current);
		}

		public TEntity Get(Guid id)
		{
			if(DIContainer.Current.Resolve<IValidator>()
				.LogIfNotSatisfied(_ => _ != Guid.Empty, id, "Guid is empty" + Environment.NewLine + Environment.StackTrace))
			{
				return GetSet().Find(id);
			}

			return null;
		}

		public IEnumerable<TEntity> GetAll()
		{
			return GetSet();
		}

		public IEnumerable<TEntity> GetPaged<KProperty>(int pageIndex, int pageCount, Expression<Func<TEntity, KProperty>> orderByExpression, bool @ascending)
		{
			var set = GetSet();

			if(ascending)
			{
				return set.OrderBy(orderByExpression)
					.Skip(pageCount * pageIndex)
					.Take(pageCount);
			}
			else
			{
				return set.OrderByDescending(orderByExpression)
						  .Skip(pageCount * pageIndex)
						  .Take(pageCount);
			}
		}

		public IEnumerable<TEntity> GetFiltered(Expression<Func<TEntity, bool>> filter)
		{
			return GetSet().Where(filter);
		}

		#endregion

		#region Private Members

		IDbSet<TEntity> GetSet()
		{
			return _unitOfWork.CreateSet<TEntity>();
		}

		#endregion
	}
}
