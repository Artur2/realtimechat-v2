﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Domain.Seedwork;

namespace Infrastructure.Seedwork
{
	public abstract class CacheRepository<TEntity> : IRepository<TEntity>
	{
		/// <summary>
		/// Returning unique key for each repository
		/// </summary>
		protected abstract string CacheRepositoryKey { get; }

		/// <summary>
		/// Getting key from source for segregation
		/// </summary>
		/// <param name="source">Some type</param>
		/// <returns></returns>
		protected abstract string GetKey(TEntity source);

		protected virtual Cache GetStorage()
		{

			if(HttpRuntime.Cache != null)
			{
				return HttpRuntime.Cache;
			}

			throw new NullReferenceException("Storage");
		}

		protected virtual IEnumerable<TEntity> GetAll<TEntity>()
		{
			var storage = GetStorage();

			if(storage == null)
			{
				throw new NullReferenceException("storage");
			}

			return storage.OfType<DictionaryEntry>()
				.Where(_ => _.Value.GetType() == typeof(TEntity))
				.Select(_ => _.Value).OfType<TEntity>();
		}

		public TEntity Get(string key)
		{
			var storage = GetStorage();

			return (TEntity)storage.Get(GetComplexKey(key));
		}

		public TEntity Get(Guid key)
		{
			throw new NotImplementedException();
		}

		public TEntity Get(object key)
		{
			throw new NotImplementedException();
		}

		public virtual void Add(TEntity item)
		{
			var storage = GetStorage();

			var key = GetComplexKey(item);

			storage.Add(key, item, null, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
		}

		public IEnumerable<TEntity> GetAll()
		{
			return GetAll<TEntity>();
		}

		public IEnumerable<TEntity> Filtered(Func<TEntity, bool> predicate)
		{
			return GetAll().Where(predicate);
		}

		public virtual void Delete(TEntity item)
		{
			var key = GetComplexKey(item);

			var storage = GetStorage();

			storage.Remove(key);
		}

		public virtual void Update(TEntity target)
		{
			var storage = GetStorage();

			var key = GetComplexKey(target);

			storage.Remove(key);

			Add(target);
		}

		private bool Exist(string key)
		{
			var storage = GetStorage();

			return storage.Get(key) != null;
		}

		private string GetComplexKey(TEntity item)
		{
			return string.Format("{0}-{1}", CacheRepositoryKey, GetKey(item));
		}

		private string GetComplexKey(string key)
		{
			return string.Format("{0}-{1}", CacheRepositoryKey, key);
		}
	}
}
