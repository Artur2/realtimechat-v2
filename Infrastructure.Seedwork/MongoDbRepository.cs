﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Domain.Seedwork;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace Infrastructure.Seedwork
{
	//todo: implement thread safety in critical sections
	public abstract class MongoDbRepository<TEntity> : IRepository<TEntity>
	{
		private static MongoClient client;
		private static MongoServer server;
		private static MongoDatabase database;
		private MongoCollection<TEntity> collection;

		protected MongoCollection<TEntity> Collection
		{
			get
			{
				return collection;
			}
		}

		protected MongoDbRepository()
		{
			var connectionString = WebConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString;

			if(String.IsNullOrWhiteSpace(connectionString))
			{
				throw new ArgumentException("ConnectionString is null or empty");
			}

			if(client == null)
			{
				client = new MongoClient(connectionString);
				server = client.GetServer();
				database = server.GetDatabase("realtimechat");
			}

			collection = database.GetCollection<TEntity>(typeof(TEntity).Name);
		}

		public abstract TEntity Get(object key);

		public virtual void Add(TEntity item)
		{
			collection.Insert(item);
		}

		public virtual IEnumerable<TEntity> GetAll()
		{
			return collection.AsQueryable<TEntity>();
		}

		public virtual IEnumerable<TEntity> Filtered(Func<TEntity, bool> predicate)
		{
			return collection.AsQueryable<TEntity>()
				.Where(predicate);
		}

		public virtual void Update(TEntity target)
		{
			collection.Save(target);
		}

		public abstract void Delete(TEntity deletingItem);
	}
}
