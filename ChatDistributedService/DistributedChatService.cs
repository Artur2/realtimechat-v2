﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ChatContext.ChatModule.DTO_s;
using Application.Seedwork;

namespace DistributedServices
{
	[ServiceApplicationErrorHandler]
	public class DistributedChatService : IDistributedChatService
	{
		//TODO: Test functionality
		public IEnumerable<ChatDTO> All(string host)
		{
			var httpClient = new HttpClient
			{
				BaseAddress = new Uri(host)
			};

			var request = httpClient.GetAsync("api/v1/chats");

			var chats = request.Result.Content.ReadAsAsync<ChatDTO[]>();

			Task.WaitAll(request,chats);

			return chats.Result;
		}

		public ChatDTO findById(Guid id, string host)
		{
			var httpClient = new HttpClient
			{
				BaseAddress = new Uri(host)
			};

			var request = httpClient.GetAsync("api/v1/chats/" + id);

			var chat = request.Result.Content.ReadAsAsync<ChatDTO>();

			Task.WaitAll(request,chat);

			return chat.Result;
		}

		public void AddChat(ChatDTO chatDto, string host)
		{
			var httpClient = new HttpClient
			{
				BaseAddress = new Uri(host)
			};

			var request = httpClient.PostAsJsonAsync("api/v1/chats", chatDto);

			Task.WaitAll(request);
		}

		public void RemoveChat(Guid id, string host)
		{
			var httpClient = new HttpClient()
			{
				BaseAddress = new Uri(host)
			};

			var request = httpClient.DeleteAsync("api/v1/chats/" + id);

			Task.WaitAll(request);
		}

		public string TestRequest()
		{
			return "Host is online";
		}
	}
}
