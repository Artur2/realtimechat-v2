﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Application.ChatContext.ChatModule.DTO_s;
using Application.Seedwork;

namespace DistributedServices
{
	[ServiceContract]
	public interface IDistributedChatService
	{

		[OperationContract]
		[FaultContract(typeof(ApplicationServiceError))]
		IEnumerable<ChatDTO> All(string host);

		[OperationContract]
		[FaultContract(typeof(ApplicationServiceError))]
		ChatDTO findById(Guid id, string host);

		[OperationContract]
		[FaultContract(typeof(ApplicationServiceError))]
		void AddChat(ChatDTO chatDto,string host);

		[OperationContract]
		[FaultContract(typeof(ApplicationServiceError))]
		void RemoveChat(Guid id,string host);

		[OperationContract]
		[FaultContract(typeof (ApplicationServiceError))]
		string TestRequest();
	}
}
