# DEMO PROJECT #

This project contains many modern technologies like:

*  ASP.NET MVC
*  Entity Framework
*  Unity DI
*  WCF(Windows Communication Foundation)
*  Windows Services
*  Web API
*  Signal-R(Realtime communication)

Also this project contains Windows Service Client which uses for distributed services
