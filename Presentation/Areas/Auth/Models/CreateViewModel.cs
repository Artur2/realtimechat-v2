﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Areas.Auth.Models
{
	public class CreateViewModel
	{
		[Required]
		public string Login { get; set; }

		[Required]
		[MinLength(6)]
		public string Password { get; set; }
	}
}