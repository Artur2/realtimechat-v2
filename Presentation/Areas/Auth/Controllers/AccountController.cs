﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;
using Domain.ChatContext.AuthModule.Aggregates.AccountAgg;
using Domain.ChatContext.AuthModule.Aggregates.AccountBaseAgg;
using Domain.Seedwork;
using Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg;
using Infrastructure.Crosscutting.DI;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Presentation.Areas.Auth.Models;

namespace Presentation.Areas.Auth.Controllers
{
	public class AccountController : Controller
	{
		private UserManager<Account> UserManager { get; set; }

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		public AccountController()
		{
			UserManager = new UserManager<Account>(DIContainer.Current.Resolve<IUserStore<Account>>());
		}

		//
		// GET: /Account/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult All()
		{
			var repo = DIContainer.Current.Resolve<IAuthRepository<Account>>();

			return View(repo.GetAll());
		}

		public ActionResult Create()
		{
			var model = new CreateViewModel();

			return View(model);
		}

		//todo imlement authorization
		public ActionResult Create(CreateViewModel model)
		{
			if(!ModelState.IsValid)
			{
				return View(model);
			}

			var account = Account.Factory.CreateNew(model.Login);

			var identity = UserManager.Create(account, model.Password);

			if (!identity.Succeeded)
			{
				throw new AuthenticationException();
			}

			return Redirect("/");
		}

		[HttpGet]
		public ActionResult Logon()
		{
			var model = new LogonViewModel();

			return View(model);
		}

		public ActionResult Logon(LogonViewModel model)
		{
			if(!ModelState.IsValid)
			{
				return View(model);
			}

			var user = UserManager.FindByName(model.Login);

			if(user != null && UserManager.HasPassword(user.Id))
			{
				var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

				AuthenticationManager.SignIn(new AuthenticationProperties()
				{
					IsPersistent = true
				}, identity);
			}

			return RedirectToAction("Index", "Base");
		}
	}
}