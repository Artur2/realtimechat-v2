﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.Practices.Unity;

namespace Presentation.Areas.Base.Controllers
{
	public class BaseController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult ChatList()
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chats = repo.GetAll();

			return View(chats);
		}

		[HttpGet]
		public ActionResult CreateChat()
		{
			var chat = Chat.Factory.CreateNew();

			return View(chat);
		}

		[HttpPost]
		public ActionResult CreateChat(Chat model)
		{
			if(!ModelState.IsValid)
			{
				return View(model);
			}

			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = Chat.Factory.CreateNew(model.Title);

			repo.Add(chat);

			return RedirectToAction("ChatList", "Base");
		}

		public ActionResult DeleteChat(Guid id)
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chatToDelete = repo.Get(id);

			repo.Delete(chatToDelete);

			return RedirectToAction("ChatList");
		}

		public ActionResult ChatRoom(Guid id)
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = repo.Get(id);

			if(chat != null)
			{
				return View(chat);
			}

			return HttpNotFound();
		}
	}
}