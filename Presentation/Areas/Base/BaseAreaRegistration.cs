﻿using System.Web.Mvc;

namespace Presentation.Areas.Base
{
	public class BaseAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Base";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute("Base_index",
				"",
				new { controller = "Base", action = "Index" });

			context.MapRoute(
				"Base_default",
				"Base/{action}/{id}",
				new { controller = "Base", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}