﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;

namespace Application.ChatContext.ChatModule.Hubs
{
	public class ChatHub : Hub
	{

		public void Send(string chatName, string name, string message)
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = repo.GetAll().FirstOrDefault(_ => _.Title.Equals(chatName));

			if(chat != null)
			{
				chat.AddMessage(Message.Factory.CreateNew(name, message));

				repo.Update(chat);
			}

			Clients.Group(chatName).BroadcastMessage(name, message);
		}

		public void JoinToChat(string chatName, string userName)
		{
			Groups.Add(Context.ConnectionId, chatName);
		}

	}
}
