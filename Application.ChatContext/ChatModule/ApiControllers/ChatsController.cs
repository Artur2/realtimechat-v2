﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Application.ChatContext.ChatModule.DTO_s;
using AutoMapper;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.Practices.Unity;

namespace Application.ChatContext.ChatModule.ApiControllers
{
	public class ChatsController : ApiController
	{
		/*
			todo: добавить авторизацию по ключу
		 	
			Способ хеширования:
			
			берется заранее определенный алгоритм 
			
			на вход подается api-key, которые шифруется с ключем поля password 
			
			пользователя, в свою очередь на выходе получается ключ для отправки в заголовках запроса
			
			encode(api-key,password) --> headers: username : encoded secret 
		  
			Сервер в свою очередь узнает пароль и api-key по имени пользователя 
			и генерирует ключ так же по api-key и паролю, далее сверяет
			Если совпадают то пользовател имеет доступ к ресурсу.
		   
			Больше информации : 
			http://www.piotrwalat.net/hmac-authentication-in-asp-net-web-api/
			http://www.asp.net/web-api/overview/security/basic-authentication
			http://blog.mirajavora.com/authenticate-web-api-using-access-tokens
		*/

		[HttpGet]
		[Route("api/v1/chats")]
		public HttpResponseMessage All()
		{
			var chats = DIContainer.Current.Resolve<IRepository<Chat>>();

			var dtos = new List<ChatDTO>();

			foreach(var chat in chats.GetAll())
			{
				dtos.Add(Mapper.Map<Chat, ChatDTO>(chat));
			}

			return Request.CreateResponse(HttpStatusCode.OK, dtos);
		}

		[HttpGet]
		[Route("api/v1/chats/{id}")]
		public HttpResponseMessage ById(Guid id)
		{
			var chats = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = chats.Filtered(_ => _.Id.Equals(id)).FirstOrDefault();

			if(chat == null)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound);
			}

			return Request.CreateResponse(HttpStatusCode.Found, Mapper.Map<Chat, ChatDTO>(chat));
		}

		[HttpPost]
		[Route("api/v1/chats")]
		public HttpResponseMessage PostChat(ChatDTO dto)
		{
			var chat = Mapper.Map<ChatDTO, Chat>(dto);

			if(chat == null)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "chat is null");
			}

			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			repo.Add(chat);

			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[HttpPut]
		[Route("api/v1/chats")]
		public HttpResponseMessage UpdateChat(ChatDTO dto)
		{
			if(dto == null)
			{
				return Request.CreateErrorResponse(HttpStatusCode.NoContent, "dto is null");
			}

			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = Mapper.Map<ChatDTO, Chat>(dto);

			var persistedChat = repo.Get(dto.Id);

			if(persistedChat != null)
			{
				persistedChat.UpdateProperties(chat);

				repo.Update(persistedChat);

				return Request.CreateResponse(HttpStatusCode.OK);
			}

			return Request.CreateErrorResponse(HttpStatusCode.NoContent, "not found chat with given key");
		}

		[HttpDelete]
		[Route("api/v1/chats/{id}")]
		public HttpResponseMessage DeleteChat(Guid id)
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			try
			{
				var chat = repo.Get(id);

				repo.Delete(chat);

				return Request.CreateResponse(HttpStatusCode.OK);
			}
			catch(Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.InternalServerError);
			}
		}
	}
}
