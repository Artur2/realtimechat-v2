﻿using System.Collections.Generic;
using System.Web.Http;
using Application.ChatContext.ChatModule.DTO_s;
using AutoMapper;
using Domain.ChatContext.AuthModule.Aggregates.UserAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.Practices.Unity;

namespace Application.ChatContext.ChatModule.ApiControllers
{
	public class UsersController : ApiController
	{
		[HttpGet]
		public IEnumerable<UserDTO> All()
		{
			var repo = DIContainer.Current.Resolve<IRepository<User>>();

			var dtos = new List<UserDTO>();

			foreach(var user in repo.GetAll())
			{
				dtos.Add(Mapper.Map<User, UserDTO>(user));
			}

			return dtos;
		}
	}
}
