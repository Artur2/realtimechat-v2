﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ChatContext.ChatModule.DTO_s
{
	public class ChatDTO
	{
		public Guid Id { get; set; }

		public string Title { get; set; }

		public IEnumerable<MessageDTO> Messages { get; set; }
	}
}
