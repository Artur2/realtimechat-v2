﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.ChatContext.ChatModule.DTO_s;
using Application.Seedwork;
using AutoMapper;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.Practices.Unity;

namespace Application.ChatContext.ChatModule.Services
{
	[ServiceApplicationErrorHandler]
	public class ChatService : IChatService
	{
		public IEnumerable<ChatDTO> All()
		{
			var chats = DIContainer.Current.Resolve<IRepository<Chat>>();

			var dtos = new List<ChatDTO>();

			foreach(var chat in chats.GetAll())
			{
				dtos.Add(Mapper.Map<Chat, ChatDTO>(chat));
			}

			return dtos;
		}

		public ChatDTO FindById(Guid id)
		{
			var chats = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = chats.Get(id);

			if(chat == null)
			{
				return null;
			}

			return Mapper.Map<Chat, ChatDTO>(chat);
		}

		public void AddChat(ChatDTO newChat)
		{
			var repo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chat = Mapper.Map<ChatDTO, Chat>(newChat);

			if(chat != null)
			{
				repo.Add(chat);
			}
		}

		public void RemoveChat(Guid id)
		{
			var chatRepo = DIContainer.Current.Resolve<IRepository<Chat>>();

			var chatToDelete = chatRepo.Get(id);

			if(chatToDelete != null)
			{
				chatRepo.Delete(chatToDelete);
			}
		}
	}
}
