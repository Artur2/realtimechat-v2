﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Application.ChatContext.ChatModule.DTO_s;
using Application.Seedwork;

namespace Application.ChatContext.ChatModule
{
	[ServiceContract]
	public interface IChatService
	{
		[OperationContract]
		[FaultContract(typeof(ServiceApplicationErrorHandler))]
		IEnumerable<ChatDTO> All();

		[OperationContract]
		[FaultContract(typeof(ServiceApplicationErrorHandler))]
		ChatDTO FindById(Guid id);

		[OperationContract]
		[FaultContract(typeof(ServiceApplicationErrorHandler))]
		void AddChat(ChatDTO newChat);

		[OperationContract]
		[FaultContract(typeof(ServiceApplicationErrorHandler))]
		void RemoveChat(Guid id);
	}
}
