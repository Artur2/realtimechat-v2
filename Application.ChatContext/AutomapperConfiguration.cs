﻿using Application.ChatContext.ChatModule.DTO_s;
using AutoMapper;
using Domain.ChatContext.AuthModule.Aggregates.UserAgg;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;

namespace Application.ChatContext
{
	public static class AutomapperConfiguration
	{
		private static volatile bool IsInitialized = false;

		public static void Configure()
		{
			if(!IsInitialized)
			{
				IsInitialized = true;

				Mapper.CreateMap<Chat, ChatDTO>();
				Mapper.CreateMap<ChatDTO, Chat>();
				Mapper.CreateMap<Message, MessageDTO>();
				Mapper.CreateMap<MessageDTO, Message>();
				Mapper.CreateMap<User, UserDTO>();
				Mapper.CreateMap<UserDTO, User>();
			}
		}
	}
}
