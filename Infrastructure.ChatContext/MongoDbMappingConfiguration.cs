﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.AccountAgg;
using Domain.Seedwork;
using Microsoft.AspNet.Identity.EntityFramework;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Infrastructure.ChatContext
{
	public static class MongoDbMappingConfiguration
	{
		private static bool configured = false;

		public static void Configure()
		{
			if(!configured)
			{
				BsonSerializer.UseNullIdChecker = true;
				BsonSerializer.UseZeroIdChecker = true;

				BsonClassMap.RegisterClassMap<Entity>(m =>
				{
					m.AutoMap();
					m.MapIdMember(_ => _.Id);
					m.IdMemberMap.SetIdGenerator(GuidGenerator.Instance);
				});

				BsonClassMap.RegisterClassMap<IdentityUser>(m =>
				{
					m.AutoMap();
					m.MapIdMember(_ => _.Id);
					m.IdMemberMap.SetRepresentation(BsonType.String);
				});

				BsonClassMap.RegisterClassMap<IdentityRole>((m) =>
				{
					m.AutoMap();
					m.MapIdMember(_ => _.Id);
					m.IdMemberMap.SetRepresentation(BsonType.String);
				});

				BsonClassMap.RegisterClassMap<IdentityUserClaim>(m =>
				{
					m.AutoMap();
					m.MapIdMember(_ => _.Id);
					m.IdMemberMap.SetRepresentation(BsonType.Int32);
				});

				BsonClassMap.RegisterClassMap<IdentityUserLogin>(m =>
				{
					m.AutoMap();
					m.MapIdMember(_ => _.UserId);
					m.IdMemberMap.SetRepresentation(BsonType.String);
				});

				configured = true;
			}
		}
	}
}
