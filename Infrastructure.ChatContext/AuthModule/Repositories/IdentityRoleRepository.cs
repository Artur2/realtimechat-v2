﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Seedwork;
using Infrastructure.Seedwork;
using Microsoft.AspNet.Identity.EntityFramework;
using MongoDB.Driver.Builders;

namespace Infrastructure.ChatContext.AuthModule.Repositories
{
	public class IdentityRoleRepository : MongoDbRepository<IdentityRole>
	{
		private static object LockObject = new object();

		public override IdentityRole Get(object key)
		{
			return base.Filtered(_ => _.Id.Equals(key)).FirstOrDefault();
		}

		public override void Add(IdentityRole item)
		{
			lock(LockObject)
			{
				item.Id = Guid.NewGuid().ToString();
			}
			base.Add(item);
		}

		public override void Delete(IdentityRole deletingItem)
		{
			Collection.Remove(Query<IdentityRole>.Where(_ => _.Id.Equals(deletingItem.Id)));
		}
	}
}
