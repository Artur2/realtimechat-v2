﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.AccountAgg;
using Infrastructure.Seedwork;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace Infrastructure.ChatContext.AuthModule.Repositories
{
	public class AccountRepository : MongoDbRepository<Account>
	{
		public override Account Get(object key)
		{
			return base.Filtered(_ => _.Id.Equals(key)).FirstOrDefault();
		}

		public override void Add(Account item)
		{
			item.Id = Guid.NewGuid().ToString();
			base.Add(item);
		}

		public override void Delete(Account deletingItem)
		{
			Collection.Remove(Query<Account>
				.Where(_ => _.Id.Equals(deletingItem.Id)));
		}
	}
}
