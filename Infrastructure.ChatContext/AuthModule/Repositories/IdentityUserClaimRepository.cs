﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Seedwork;
using Infrastructure.Seedwork;
using Microsoft.AspNet.Identity.EntityFramework;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace Infrastructure.ChatContext.AuthModule.Repositories
{
	public class IdentityUserClaimRepository : MongoDbRepository<IdentityUserClaim>
	{
		private static object LockObject = new object();

		public override IdentityUserClaim Get(object key)
		{
			return base.Filtered(_ => _.Id.Equals(key)).FirstOrDefault();
		}

		public override void Add(IdentityUserClaim item)
		{
			lock (LockObject)
			{
				item.Id = Collection.AsQueryable().Max(_ => _.Id);	
			}

			base.Add(item);
		}

		public override void Delete(IdentityUserClaim deletingItem)
		{
			Collection.Remove(Query<IdentityUserClaim>.Where(_ => _.Id == deletingItem.Id
																  && _.ClaimType.Equals(deletingItem.ClaimType)
																  && _.ClaimValue.Equals(deletingItem.ClaimValue)));
		}
	}
}
