﻿using System;
using System.Collections.Generic;
using Domain.ChatContext.AuthModule.Aggregates.AccountBaseAgg;
using Domain.Seedwork;
using Infrastructure.Seedwork;

namespace Infrastructure.ChatContext.AuthModule.Repositories
{
	public class AccountBaseRepository : CacheRepository<AccountBase>
	{
		protected override string CacheRepositoryKey
		{
			get { return "AccountBase"; }
		}

		protected override string GetKey(AccountBase source)
		{
			return source.Id;
		}
	}
}
