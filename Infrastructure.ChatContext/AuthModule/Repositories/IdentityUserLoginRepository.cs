﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Seedwork;
using Infrastructure.Seedwork;
using Microsoft.AspNet.Identity.EntityFramework;
using MongoDB.Driver.Builders;

namespace Infrastructure.ChatContext.AuthModule.Repositories
{
	public class IdentityUserLoginRepository : MongoDbRepository<IdentityUserLogin>
	{
		public override IdentityUserLogin Get(object key)
		{
			return base.Filtered(_ => _.UserId.Equals(key)).FirstOrDefault();
		}

		public override void Add(IdentityUserLogin item)
		{
			item.UserId = item.User.Id;
			base.Add(item);
		}

		public override void Delete(IdentityUserLogin deletingItem)
		{
			Collection.Remove(Query<IdentityUserLogin>.Where(_ => _.LoginProvider.Equals(deletingItem.LoginProvider)
																  && _.ProviderKey.Equals(deletingItem.ProviderKey)
																  && _.UserId.Equals(deletingItem.UserId)));
		}
	}
}
