﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.UserAgg;
using Domain.Seedwork;
using Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Infrastructure.ChatContext.AuthModule.Stores
{
	public class AccountStore : IUserLoginStore<IUser>, IUserClaimStore<IUser>, IUserRoleStore<IUser>, IUserSecurityStampStore<IUser>
	{
		private HashSet<Account> _accounts;

		public AccountStore(IRepository<User> repository)
		{
			
		}

		public void Dispose()
		{
			_accounts = null;
		}

		public Task CreateAsync(Account user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Add(user);
			});
		}

		public Task UpdateAsync(Account user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Update(user);
			});
		}

		public Task DeleteAsync(Account user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Delete(user);
			});
		}

		public Task<Account> FindByIdAsync(string userId)
		{
			return Task<Account>.Factory.StartNew(() =>
			{
				return _accounts.Get(userId);
			});
		}

		public Task<Account> FindByNameAsync(string userName)
		{
			return Task<Account>.Factory.StartNew(() =>
			{
				return _accounts.Filtered(_ => _.UserName.Equals(userName))
					.FirstOrDefault();
			});
		}

		public Task AddLoginAsync(Account user, UserLoginInfo login)
		{
			return Task.Factory.StartNew(() =>
			{
				var identityLogin = new IdentityUserLogin()
				{
					LoginProvider = login.LoginProvider,
					ProviderKey = login.ProviderKey,
					User = user,
					UserId = user.Id
				};

				user.Logins.Add(identityLogin);

				_accounts.Update(user);
			});
		}

		public Task RemoveLoginAsync(Account user, UserLoginInfo login)
		{
			return Task.Factory.StartNew(() =>
			{
				var identityLogin = new IdentityUserLogin()
				{
					LoginProvider = login.LoginProvider,
					ProviderKey = login.ProviderKey,
					User = user,
					UserId = user.Id
				};

				user.Logins.Remove(identityLogin);

				_accounts.Update(user);
			});
		}

		public Task<IList<UserLoginInfo>> GetLoginsAsync(Account user)
		{
			return Task<IList<UserLoginInfo>>.Factory.StartNew(() =>
			{
				var logins = new List<UserLoginInfo>();

				foreach(var identityUserLogin in user.Logins)
				{
					var login = new UserLoginInfo(identityUserLogin.LoginProvider, identityUserLogin.ProviderKey);

					logins.Add(login);
				}

				return logins;
			});
		}

		public Task<Account> FindAsync(UserLoginInfo login)
		{
			return Task<Account>.Factory.StartNew(() =>
			{
				return
					_accounts.Filtered(
						_ => _.Logins.Any(o => o.LoginProvider.Equals(login.ProviderKey) && o.ProviderKey.Equals(login.ProviderKey)))
						.FirstOrDefault();
			});
		}

		public Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(Account user)
		{
			return Task<IList<System.Security.Claims.Claim>>.Factory.StartNew(() =>
			{
				var claims = new List<System.Security.Claims.Claim>();

				foreach(var identityUserClaim in user.Claims)
				{
					claims.Add(new System.Security.Claims.Claim(identityUserClaim.ClaimType, identityUserClaim.ClaimValue));
				}

				return claims;
			});
		}

		public Task AddClaimAsync(Account user, Claim claim)
		{
			return Task.Factory.StartNew(() =>
			{
				user.Claims.Add(new IdentityUserClaim()
				{
					ClaimType = claim.Type,
					ClaimValue = claim.Value,
					User = user
				});

				_accounts.Update(user);
			});
		}

		public Task RemoveClaimAsync(Account user, Claim claim)
		{
			return Task.Factory.StartNew(() =>
			{
				var identityClaim = new IdentityUserClaim()
				{
					ClaimType = claim.Type,
					ClaimValue = claim.Value,
					User = user
				};

				user.Claims.Remove(identityClaim);

				_accounts.Update(user);
			});
		}

		public Task AddToRoleAsync(Account user, string role)
		{
			return Task.Factory.StartNew(() =>
			{
				var identityRole = new IdentityUserRole()
				{
					Role = new IdentityRole(role),
					User = user,
				};

				user.Roles.Add(identityRole);

				_accounts.Update(user);
			});
		}

		public Task RemoveFromRoleAsync(Account user, string role)
		{
			return Task.Factory.StartNew(() =>
			{
				var removeRole = user.Roles.FirstOrDefault(_ => _.Role.Name.Equals(role));

				if(removeRole != null)
				{
					user.Roles.Remove(removeRole);

					_accounts.Update(user);
				}
			});
		}

		public Task<IList<string>> GetRolesAsync(Account user)
		{
			return Task<IList<string>>.Factory.StartNew(() =>
			{
				var roles = new List<string>();

				foreach(var identityUserRole in user.Roles)
				{
					roles.Add(identityUserRole.Role.Name);
				}

				return roles;
			});
		}

		public Task<bool> IsInRoleAsync(Account user, string role)
		{
			return Task<bool>.Factory.StartNew(() =>
			{
				if(user.Roles != null)
				{
					return user.Roles.Any(_ => _.Role.Name.Equals(role));
				}

				return false;
			});
		}

		public Task SetSecurityStampAsync(Account user, string stamp)
		{
			return Task.Factory.StartNew(() => user.SecurityStamp = stamp);
		}

		public Task<string> GetSecurityStampAsync(Account user)
		{
			return Task.Factory.StartNew(() => user.SecurityStamp);
		}
	}
}
