﻿using System;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.AccountBaseAgg;
using Microsoft.AspNet.Identity;

namespace Infrastructure.ChatContext.AuthModule.Stores
{
	class RoleBaseStore : IRoleStore<RoleBase>
	{
		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public Task CreateAsync(RoleBase role)
		{
			throw new NotImplementedException();
		}

		public Task UpdateAsync(RoleBase role)
		{
			throw new NotImplementedException();
		}

		public Task DeleteAsync(RoleBase role)
		{
			throw new NotImplementedException();
		}

		public Task<RoleBase> FindByIdAsync(string roleId)
		{
			throw new NotImplementedException();
		}

		public Task<RoleBase> FindByNameAsync(string roleName)
		{
			throw new NotImplementedException();
		}
	}
}
