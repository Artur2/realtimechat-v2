﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.AccountBaseAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.DI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;

namespace Infrastructure.ChatContext.AuthModule.Stores
{
	public class AccountBaseStore : IUserLoginStore<AccountBase>, IUserClaimStore<AccountBase>, IUserRoleStore<AccountBase>, IUserPasswordStore<AccountBase>, IUserSecurityStampStore<AccountBase>
	{
		private IRepository<AccountBase> _accounts;
		private IRepository<IdentityRole> _roles;
		private IRepository<IdentityUserClaim> _claims;
		private IRepository<IdentityUserLogin> _logins;

		public AccountBaseStore()
		{
			_accounts = DIContainer.Current.Resolve<IRepository<AccountBase>>();
			_roles = DIContainer.Current.Resolve<IRepository<IdentityRole>>();
			_claims = DIContainer.Current.Resolve<IRepository<IdentityUserClaim>>();
			_logins = DIContainer.Current.Resolve<IRepository<IdentityUserLogin>>();
		}

		public void Dispose()
		{
			_accounts = null;
		}

		public Task CreateAsync(AccountBase user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Add(user);
			});
		}

		public Task UpdateAsync(AccountBase user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Update(user);
			});
		}

		public Task DeleteAsync(AccountBase user)
		{
			return Task.Factory.StartNew(() =>
			{
				_accounts.Delete(user);
			});
		}

		public Task<AccountBase> FindByIdAsync(string userId)
		{
			return Task<AccountBase>.Factory.StartNew(() =>
			{
				return _accounts.GetAll().FirstOrDefault(_ => _.Id.Equals(userId));
			});
		}

		public Task<AccountBase> FindByNameAsync(string userName)
		{
			return Task<AccountBase>.Factory.StartNew(() =>
			{
				return _accounts.GetAll().FirstOrDefault(_ => _.UserName.Equals(userName));
			});
		}

		public Task AddLoginAsync(AccountBase user, UserLoginInfo login)
		{
			throw new NotImplementedException();
		}

		public Task RemoveLoginAsync(AccountBase user, UserLoginInfo login)
		{
			throw new NotImplementedException();
		}

		public Task<IList<UserLoginInfo>> GetLoginsAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}

		public Task<AccountBase> FindAsync(UserLoginInfo login)
		{
			throw new NotImplementedException();
		}

		public Task<IList<Claim>> GetClaimsAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}

		public Task AddClaimAsync(AccountBase user, Claim claim)
		{
			throw new NotImplementedException();
		}

		public Task RemoveClaimAsync(AccountBase user, Claim claim)
		{
			throw new NotImplementedException();
		}

		public Task AddToRoleAsync(AccountBase user, string role)
		{
			throw new NotImplementedException();
		}

		public Task RemoveFromRoleAsync(AccountBase user, string role)
		{
			throw new NotImplementedException();
		}

		public Task<IList<string>> GetRolesAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}

		public Task<bool> IsInRoleAsync(AccountBase user, string role)
		{
			throw new NotImplementedException();
		}

		public Task SetPasswordHashAsync(AccountBase user, string passwordHash)
		{
			throw new NotImplementedException();
		}

		public Task<string> GetPasswordHashAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}

		public Task<bool> HasPasswordAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}

		public Task SetSecurityStampAsync(AccountBase user, string stamp)
		{
			throw new NotImplementedException();
		}

		public Task<string> GetSecurityStampAsync(AccountBase user)
		{
			throw new NotImplementedException();
		}
	}
}
