﻿using Domain.ChatContext.AuthModule.Aggregates.UserAgg;
using Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg
{
	public class AccountFactory
	{
		public Account CreateNew(User user)
		{
			var acc = new Account()
			{
				UserName = user.FullName
			};

			foreach(var claim in user.Claims)
			{
				acc.Claims.Add(new IdentityUserClaim() { ClaimType = claim.ClaimType, ClaimValue = claim.ClaimValue });
			}

			foreach(var userRole in user.Roles)
			{
				acc.Roles.Add(new IdentityUserRole()
				{
					Role = new IdentityRole()
					{
						Id = userRole.Role.Id.ToString(), 
						Name = userRole.Role.Name
					},
					User = new IdentityUser()
					{
							
					}
				});
			}
		}
	}
}
