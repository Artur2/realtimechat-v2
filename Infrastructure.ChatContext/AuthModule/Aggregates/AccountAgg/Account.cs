﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg
{
	public class Account : IdentityUser
	{

		//todo: implement decarator pattern with "User" Entity

		private static AccountFactory _factory = new AccountFactory();

		public static AccountFactory Factory
		{
			get
			{
				return _factory;
			}
		}
	}
}
