﻿namespace Infrastructure.ChatContext
{

	public static class RepositoryConfiguration
	{

		private static volatile bool IsInitialized = false;

		public static void Configure()
		{
	
			if(!IsInitialized)
			{
				IsInitialized = true;
			}
			else
			{
				return;
			}

			MongoDbMappingConfiguration.Configure();
			//InitializeUsers();
		}
	}
}
