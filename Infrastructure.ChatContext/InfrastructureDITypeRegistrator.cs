﻿using Domain.ChatContext.AuthModule.Aggregates.UserAgg;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.ChatContext.AuthModule.Aggregates.AccountAgg;
using Infrastructure.ChatContext.AuthModule.Repositories;
using Infrastructure.ChatContext.AuthModule.Stores;
using Infrastructure.ChatContext.UnitOfWork;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Seedwork;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace Infrastructure.ChatContext
{
	public class InfrastructureDITypeRegistrator : IUnityContainerTypeRegistrationProvider
	{
		public void Register(IUnityContainer container)
		{
			container.RegisterType<IQueryableUnitOfWork, MainBCUnitOfWork>();
			container.RegisterType<IRepository<Chat>, Repository<Chat>>();
			container.RegisterType<IRepository<User>, Repository<User>>();
			container.RegisterType<IAuthRepository<Account>, AuthRepository>();

			//container.RegisterType<IRepository<AccountBase>, AccountBaseRepository>();
			//container.RegisterType<IUserStore<Account>, AccountStore>();
			//container.RegisterType<IRepository<IdentityRole>, IdentityRoleRepository>(); 
			//container.RegisterType<IRepository<IdentityUserClaim>, IdentityUserClaimRepository>(); 
			//container.RegisterType<IRepository<IdentityUserLogin>, IdentityUserLoginRepository>();
		}
	}
}
