﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Domain.Seedwork;
using Infrastructure.Seedwork;
using MongoDB.Driver.Builders;

namespace Infrastructure.ChatContext.ChatModule.Repositories
{

	public class ChatRepository : MongoDbRepository<Chat>
	{
		public override Chat Get(object key)
		{
			return base.Filtered(_ => _.Id.Equals(key))
				.FirstOrDefault();
		}

		public override void Delete(Chat deletingItem)
		{
			Collection.Remove(Query<Chat>.Where(_ => _.Id.Equals(deletingItem.Id)));
		}
	}
}
