﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.ChatContext.ChatModule.Aggregates.UserAgg;
using Domain.Seedwork;
using Infrastructure.Seedwork;
using MongoDB.Driver.Builders;

namespace Infrastructure.ChatContext.ChatModule.Repositories
{

	public class UserRepository : MongoDbRepository<User>
	{
		public override User Get(object key)
		{
			return base.Filtered(_ => _.Id.Equals(key)).FirstOrDefault();
		}

		public override void Delete(User deletingItem)
		{
			Collection.Remove(Query<User>.Where(_ => _.Id.Equals(deletingItem.Id)));
		}
	}
}
