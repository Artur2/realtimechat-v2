﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;
using Infrastructure.ChatContext.UnitOfWork.Mappings;
using Infrastructure.Crosscutting.DI;
using Infrastructure.Crosscutting.Validation;
using Infrastructure.Seedwork;
using Microsoft.Practices.Unity;

namespace Infrastructure.ChatContext.UnitOfWork
{
	public class MainBCUnitOfWork : DbContext, IQueryableUnitOfWork
	{

		#region IDbSet Members

		private IDbSet<Chat> _chats;

		public IDbSet<Chat> Chats
		{
			get
			{
				if(DIContainer.Current.Resolve<IValidator>().LogIfNotSatisfied(_ => _ != null, _chats, "Chats is null"))
				{
					return _chats;
				}

				_chats = base.Set<Chat>();

				return _chats;
			}
		}

		private IDbSet<Message> messages;

		public IDbSet<Message> Messages
		{
			get
			{
				if(DIContainer.Current.Resolve<IValidator>().LogIfNotSatisfied(_ => _ != null, _chats, "Messages is null"))
				{
					return messages;
				}

				messages = base.Set<Message>();
			}
		}


		#endregion

		#region IQueryableUnitOfWork Members

		public void Commit()
		{
			SaveChanges();
		}

		public void CommitAndRefreshChanges()
		{
			var saveFailed = false;

			do
			{
				try
				{
					SaveChanges();

					saveFailed = false;
				}
				catch(DbUpdateConcurrencyException exception)
				{
					saveFailed = true;

					exception.Entries.ToList()
						.ForEach(_ =>
						{
							_.OriginalValues.SetValues(_.GetDatabaseValues());
						});

				}

			} while(saveFailed);
		}

		public void RollbackChanges()
		{
			ChangeTracker.Entries().ToList()
				.ForEach(_ => _.State = EntityState.Unchanged);
		}

		public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
		{
			return Database.SqlQuery<TEntity>(sqlQuery, parameters);
		}

		public int ExecuteCommand(string sqlCommand, params object[] parameters)
		{
			return Database.ExecuteSqlCommand(sqlCommand, parameters);
		}

		public IDbSet<TEntity> CreateSet<TEntity>() where TEntity : class
		{
			return Set<TEntity>();
		}

		public void Attach<TEntity>(TEntity item) where TEntity : class
		{
			Entry(item).State = EntityState.Modified;
		}

		public void SetModified<TEntity>(TEntity item) where TEntity : class
		{
			Entry(item).State = EntityState.Modified;
		}

		public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
		{
			Entry(original).CurrentValues.SetValues(current);
		}

		#endregion

		#region Ovverides

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			Database.SetInitializer(new CreateDatabaseIfNotExists<MainBCUnitOfWork>());

			modelBuilder.Configurations.Add(new ChatEntityTypeConfiguration());
			modelBuilder.Configurations.Add(new MessageEntityTypeConfiguration());

			base.OnModelCreating(modelBuilder);
		}

		#endregion
	}
}

