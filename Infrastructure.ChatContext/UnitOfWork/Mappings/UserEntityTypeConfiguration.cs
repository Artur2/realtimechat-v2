﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.UserAgg;

namespace Infrastructure.ChatContext.UnitOfWork.Mappings
{
	public class UserEntityTypeConfiguration : EntityTypeConfiguration<User>
	{
		public UserEntityTypeConfiguration()
		{
			HasKey(_ => _.Id);
		}
	}
}
