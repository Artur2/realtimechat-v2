﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.ChatModule.Aggregates.ChatAgg;

namespace Infrastructure.ChatContext.UnitOfWork.Mappings
{
	public class ChatEntityTypeConfiguration : EntityTypeConfiguration<Chat>
	{
		//todo: Check migrations

		public ChatEntityTypeConfiguration()
		{
			HasKey(_ => _.Id);

			Property(_ => _.Title)
				.IsRequired();

			HasMany(_ => _.Messages);
		}
	}
}
