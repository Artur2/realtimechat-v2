﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using Domain.Seedwork;

namespace Domain.ChatContext.ChatModule.Aggregates.ChatAgg
{
	/// <summary>
	/// Chat Domain Entity
	/// </summary>
	public class Chat : Entity
	{

		private static readonly ChatFactory _factory = new ChatFactory();
		
		public static ChatFactory Factory
		{
			get
			{
				return _factory;
			}
		}

		[Required]
		public string Title { get; set; }

		public HashSet<Message> Messages { get; set; }

		public void SetTitle(string title)
		{
			Title = title;
		}

		public void AddMessage(Message message)
		{
			Messages.Add(message);
		}

		public void AddMessageRange(IEnumerable<Message> messages)
		{
			foreach(var message in messages)
			{
				Messages.Add(message);
			}
		}

		public void SetMessageRange(IEnumerable<Message> messages)
		{
			Messages = new HashSet<Message>(messages);
		}
	}
}
