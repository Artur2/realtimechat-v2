﻿using System;
using Domain.Seedwork;

namespace Domain.ChatContext.ChatModule.Aggregates.ChatAgg
{
	public class Message : Entity
	{
		public static readonly MessageFactory _factory = new MessageFactory();

		public static MessageFactory Factory
		{
			get
			{
				return _factory;
			}
		}

		public string UserName { get; set; }

		public string Text { get; set; }

		public DateTime Timestamp { get; set; }

		public void SetUserName(string username)
		{
			UserName = username;
		}

		public void SetText(string messageText)
		{
			Text = messageText;
		}

		public void SetTimestamp()
		{
			Timestamp = DateTime.Now;
		}
	}
}
