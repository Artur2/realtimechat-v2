﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Domain.ChatContext.ChatModule.Aggregates.ChatAgg
{

	public class ChatFactory
	{
		public Chat CreateNew()
		{
			return CreateNew(string.Empty);
		}

		public Chat CreateNew(string title)
		{
			return CreateNew(title, new Collection<Message>());
		}

		public Chat CreateNew(string title, ICollection<Message> messages)
		{
			var chat = new Chat();

			chat.SetTitle(title);

			chat.SetMessageRange(messages);

			return chat;
		}
	}
}
