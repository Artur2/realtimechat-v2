﻿namespace Domain.ChatContext.ChatModule.Aggregates.ChatAgg {

	public class MessageFactory {

		public Message CreateNew( string userName, string text ) {

			var message = new Message();
			message.SetUserName( userName );
			message.SetText( text );
			message.SetTimestamp();

			return message;
		}
	}
}
