﻿using System.ComponentModel.DataAnnotations;
using Domain.Seedwork;

namespace Domain.ChatContext.AuthModule.Aggregates.UserAgg
{
	public class Claim : Entity
	{
		[Required]
		public string ClaimType { get; set; }

		[Required]
		public string ClaimValue { get; set; }
		
		[Required]
		public User User { get; set; }
	}
}
