﻿namespace Domain.ChatContext.AuthModule.Aggregates.UserAgg
{
	
	public class UserFactory {

		public User CreateNew( string userName ) {
			var user = new User();
			user.SetFullName( userName );

			return user;
		}
	}
}
