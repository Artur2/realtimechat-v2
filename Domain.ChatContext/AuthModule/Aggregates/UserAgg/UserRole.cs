﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ChatContext.AuthModule.Aggregates.RoleAgg;
using Domain.Seedwork;

namespace Domain.ChatContext.AuthModule.Aggregates.UserAgg
{
	public class UserRole : Entity
	{
		[Required]
		public string UserId { get; set; }

		[Required]
		public string RoleId { get; set; }

		[Required]
		public User User { get; set; }

		[Required]
		public Role Role { get; set; }
	}
}
