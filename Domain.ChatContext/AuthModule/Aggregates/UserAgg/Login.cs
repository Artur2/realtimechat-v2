﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Seedwork;

namespace Domain.ChatContext.AuthModule.Aggregates.UserAgg
{
	public class Login : Entity
	{
		public string LoginProvider { get; set; }

		public string ProviderKey { get; set; }

		public string UserId { get; set; }

		[Required]
		public User User { get; set; }
	}
}
