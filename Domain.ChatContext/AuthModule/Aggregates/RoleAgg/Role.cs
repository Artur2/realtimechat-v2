﻿using System.ComponentModel.DataAnnotations;
using Domain.Seedwork;

namespace Domain.ChatContext.AuthModule.Aggregates.RoleAgg
{
	public class Role : Entity
	{
		[Required]
		public string Name { get; set; }
	}
}
